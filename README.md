# XmodeSocial
XmodeSocial coding test

## Android
### Task #1 (Optional) - Triggering action when user taps a single word in one TextView
*Your goal is to implement the logic necessary to trigger two actions (implemented in Task #2) when the user touches the word "Hello" in
"Hello World!"
You are only permitted to add 'android:id="@+id/id_text_view"' to the existing TextView
No other additions/modifications to the activity_main.xml layout are allowed*

**Answer:**

[Source code link to make "Hello" Clickable](https://bitbucket.org/pritya/xmodesocial/src/f3118583b963d4ea823d495f311224ec59241bd6/app/src/main/java/org/pcc/xmodesocial/MainActivity.kt#lines-35:36)


### Task #2 - Actions: Toast, Scheduling work
- *If you completed Task #1:
Toast a message indicating that task #1 was successfully completed*

**Answer:**

[Source code link to toasting message](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/MainActivity.kt#lines-41)

- *Implement logic required to schedule work (Task #3) in the background approximately every hour, even when the app is closed and the phone is restarted
Your implementation does not have to execute background work at exactly 1-hour intervals
Your debug build variant should change the interval from 1-hour to 1-minute*

**Answer:**

Added dependencies in `build.gradle`
```
implementation "androidx.work:work-runtime:$versions.work"
```

[Source code link to scheduling work](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/MainActivity.kt#lines-63)

I'm using WorkMananger to schedule work to retrieve last known location periodically. 
I created two APKs for Debug and Release modes to test both time intervals.
I've added a debug status notification in addition to periodic toast, to make sure than last known location is being
retrieved without fail. 
This debug status notification, has three parts : 
- First part: Displays minutes of the hours when last know location was retrieved.
- Second part: Debug flag. true|false depending whether APK is Debug or Release. This is helpful in testing both APKs together 
at same time and at the same device. Debug apk will send notification more frequently than release apk (1 hour reliably)
- Third part: Last know location object (toString())

[Source code link to debug status notification](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/workmanager/LocationHelper.kt#lines-44)

[Click to check out Notifications screenshot](https://bitbucket.org/pritya/xmodesocial/src/master/screenshots/notifications.jpg)

***Challenge encountered:*** WorkManager is great for scheduling background work for retrieving last known location. It's reliable
and guarantees execution. However, Periodic work tasks has a minimum of 15 minutes for a reliable execution. I've added
1 minute interval for sake of completeness of task in Debug mode, but it may not fire exactly at 1 minute. It varies from couple of 
minuted to about 10-15 minutes sometimes. In order to make sure location retrieval work is executed, I added extra status
notification in addition to toast. Also, I tried another solution using AlarmManger with `ALARM_TYPE_ELAPSED` property,
which is another possible not too battery heavy/drainy solution. However, exact one minute can be achieved by using `ALARM_TYPE_RTC`
property by forcing alarm to go off and retrieve last location every one minute. Waking device too often and access location 
is not a very good solution for battery life.



### Task #3 - Recurring work implementation
*The recurring work done in the background will be to retrieve the last known (consented) location and toast the location object
(.toString())
You may choose the method you believe is best
(Optional) Explain in 1-2 lines (max) why you selected your method of collecting location data*

**Answer:**

[Source code link to retrieving last known location](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/workmanager/LocationHelper.kt#lines-25)

I used `LocationServices.getFusedLocationProviderClient(context)`. 
I picked fused location because it provides last known location using both GPS and network providers (WiFi, cellular).

I added following permission in AndroidManifest.xml:
```
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
```

Added dependencies in `build.gradle`
```
implementation "com.google.android.gms:play-services-location:$versions.location"
```

[Source code link to toasting the last known location object](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/workmanager/LocationHelper.kt#lines-42)


### Task #4 - Local storage options
*Add a TODO comment after toasting the last known location that describes at least 2 options you’d have for persisting the locations to
storage*

**Answer:**

[Source code link to TODO](https://bitbucket.org/pritya/xmodesocial/src/master/app/src/main/java/org/pcc/xmodesocial/workmanager/LocationHelper.kt#lines-52)

For persisting last known location, there are couple of options based on how many last locations (data) that we want to 
persist. If total last known locations data is about 1.5M and under, then SharedPreference will do just fine.
However, if we want to store larger set of data or probably other related user specific information in addition to only
last known location, then local database will be preferred option. Few choices for local database can be SQLite using Room
or Realm-android.


### Task #5 (Optional)
*When you finish the above tasks and are ready to send over your work, please indicate if you would like feedback from our Android
Engineer (that’s me, Justin)*

Yes, please. I'll appreciate your feedback. Thanks !

## Backend
### Querying (required)
Given the following DDL:
```
create table app.data
(
 id bigint unsigned auto_increment,
 user_id varchar(255) null,
 device_model varchar(255) null,
 created_at timestamp(6) default CURRENT_TIMESTAMP(6) not null,
 primary key (id, created_at),
 constraint ux_userid_createdat
 unique (user_id, created_at)
)
engine=InnoDB charset=utf8
;
create index ix_user_id
 on app.data (user_id)
;
```
*Write an SQL query using the DDL above to determine how many users have each device_model that were seen between
01-01-2018 and 01-07-2018 that were also not seen after 01-07-2018.*


**Answer:** 

We are trying to find devices models and corresponding user count per device model which were seen only in 
first week of Jan’2018 and haven't seen afterwords.

This is how the query will look like:
```
SELECT device_model, COUNT(device_model) as users
FROM `app.data` 
WHERE created_at BETWEEN '01-01-2018' AND '01-07-2018'
AND device_model NOT IN (SELECT device_model FROM `app.data` WHERE created_at > 01-07-2018)
GROUP BY device_model
```

I created the sample database and ran query against dummy data using an online sqllite tool.
Preparing the database:
```
create table app
(
 id bigint unsigned auto_increment,
 user_id varchar(255) null,
 device_model varchar(255) null,
 created_at timestamp(6) default CURRENT_TIMESTAMP(6) not null,
 primary key (id, created_at),
 constraint ux_userid_createdat
 unique (user_id, created_at)
)
engine=InnoDB charset=utf8
;
INSERT INTO app VALUES (1, "u1", "w", "2018-01-01");
INSERT INTO app VALUES (2, "u2", "y", "2018-01-02");
INSERT INTO app VALUES (3, "u1", "z", "2018-01-08");
INSERT INTO app VALUES (4, "u2", "x", "2018-01-07");
INSERT INTO app VALUES (5, "u1", "y", "2018-01-09");
INSERT INTO app VALUES (6, "u1", "w", "2018-01-02");
```
Running the query:
```
SELECT device_model, COUNT(device_model) as users 
FROM `app` 
WHERE created_at BETWEEN '2018-01-01' AND '2018-01-07'
AND device_model 
NOT IN 
(SELECT device_model FROM `app` WHERE created_at > '2018-01-07')
GROUP BY device_model
```

Output:
```
| device_model | users |
| -----------  | ----- |
|   w	       |   2   |
|   x	       |   1   |

```
[Click to checkout querying screenshot](https://bitbucket.org/pritya/xmodesocial/src/master/screenshots/query.jpg)

### Architecture (Optional)
*Collecting location on the backend*
- *Let’s assume that we have an app that starts at 1 million DAU (daily active users) and grows to 100 million over the course of a year*
- *Describe how you would architect a backend to collect location data with the following information:*
    - *1-100 million DAU*
    - *Location update interval per user: 15-seconds to 10-minutes*
    
  
**Answer:**

Calculating number of requests per seconds expected to hit servers:
    
Daily Requests per 15 seconds (worst case): (40 times more/worse than 10 minutes scenario)
- 1 User : 5760 x 1 = 5760 requests per day = 0.0666 request per second
- 100 million users: 0.0666 x 100 million requests = 6.66 million requests per seconds

Daily Requests per 10 minutes (best case): 
- 1 User : 144 x 1 = 144 requests per day = 0.001666 reqs/second
- 100 million users: 0.001666 x 100 million = 0.166 million reqs/sec

My Assumptions: 
- Different regions/cloud servers
- Region based DNS resolution to hit the local regions API gateway
- Total regions = 10 (assumption for simplicity)
- For one region, I should handle 0.01 million to 0.6 million req/sec
- Assuming 1 c4xlarge instance can support 0.01 million req/sec, I would need to configure auto-scaling of 1-40 instances per region.
- I would need 400 instances of web servers deployed - 40 per region for 10 regions.

I've put together a rough architecture diagram to show component interactions.

[Click to checkout architecture diagram](https://bitbucket.org/pritya/xmodesocial/src/master/screenshots/architecture.jpg)
    


## Final Instructions:

### APK (debug)
*Please provide a link to an APK of the DEBUG variant (so that I can test the 1-minute interval) of your application*

- [Debug APK](https://drive.google.com/file/d/1x_mlXl3oFv5mlwINg_bb9C28Tj5bs-ig/view?usp=sharing) Not exact 1 minute time interval. Range from couple of minutes to 5-10 minutes.
- [Release APK](https://drive.google.com/file/d/1RDhOPDuA-PWlNyBgOtveHe_kn5qC5aSn/view?usp=sharing) Exact (almost) 1 hour time interval

[Click to check out Notifications screenshot](https://bitbucket.org/pritya/xmodesocial/src/master/screenshots/notifications.jpg)

Note: I had to import private Github repo into Bitbucket. (Github doesn't allow adding collaborators unless they've Github account!)

### Provide repository access

Bitbucket repo link is [here](https://bitbucket.org/pritya/xmodesocial.git)