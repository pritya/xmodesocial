package org.pcc.xmodesocial

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import org.pcc.xmodesocial.alarm.AlarmHelper
import org.pcc.xmodesocial.workmanager.LocationHelper
import org.pcc.xmodesocial.workmanager.LocationWorker
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    companion object {
        const val PERMISSION_LOCATION = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Make "Hello" Clickable
        task1()
    }

    //Task#1 :Make "Hello" Clickable
    private fun task1() {
        val textView = findViewById<TextView>(R.id.id_text_view)
        var spannableString = SpannableString(textView.text)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(this@MainActivity, "Task #1 successfully completed.", Toast.LENGTH_SHORT).show()
            }
        }

        spannableString.setSpan(clickableSpan, 0, 5, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        textView.text = spannableString
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    //Task#2: Check permissions here and schedule last known location retrieval work
    public override fun onStart() {
        super.onStart()

        //Task #2 & #3: Scheduling
        if (!LocationHelper.checkPermissions(this)) {
            LocationHelper.askForConsent(this)
        } else {
            scheduleWork()
        }
    }

    //Task#2: Schedule work in background
    private fun scheduleWork() {
        scheduleWorkManager()

        //Following code is only for demonstration purpose of me trying another way to get a 1 minute interval
        /*if (BuildConfig.DEBUG) {
            //Unfortunately, 15minutes is minimum time interval for PeriodicWorkRequest.
            //Trying out AlarmManager with Time Elapsed property to execute this case.
            //However, 15 mins seems to be minimum reliable interval unless RTC is used
            AlarmHelper.enableAlarm(applicationContext)
        } else {
            scheduleWorkManager()
        }*/
    }

    private fun scheduleWorkManager() {
        var repeatInterval = 60L //in release mode

        //Logic to set 1 minute time interval for last location Toast.
        //However it's not reliable since PeriodicWorkRequest's minimum interval is 15 minutes.
        if (BuildConfig.DEBUG) {
            repeatInterval = 1L //Debug mode.
        }

        val mWorkManager = WorkManager.getInstance()
        val periodicBuilder =
            PeriodicWorkRequest.Builder(LocationWorker::class.java, repeatInterval, TimeUnit.MINUTES)
        val myWork = periodicBuilder.addTag(LocationWorker.LOCATION_WORKER_TAG).build()
        //keep existing periodic scheduling
        mWorkManager.enqueueUniquePeriodicWork(
            LocationWorker.LOCATION_WORKER_TAG,
            ExistingPeriodicWorkPolicy.KEEP, myWork
        )
    }

    //Handles user permissions. If User gives consent, then schedule location retrieval work,
    //otherwise ask for permission again
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scheduleWork()
            }
            else {
                Toast.makeText(this, "Location Permissions not granted.", Toast.LENGTH_LONG).show()
                LocationHelper.askForConsent(this)
            }
        }
    }
}
