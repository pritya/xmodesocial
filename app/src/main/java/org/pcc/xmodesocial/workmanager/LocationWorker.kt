package org.pcc.xmodesocial.workmanager

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import org.pcc.xmodesocial.workmanager.LocationHelper.getLastKnownLocation

class LocationWorker(context: Context,
                      workerParams: WorkerParameters) : Worker(context, workerParams) {
    companion object {
        //TAG for WorkManager to keep track on periodic task without duplicating and/or removing it
        const val LOCATION_WORKER_TAG = "LOCATION_WORKER_TAG"

    }

    //Get last know location
    override fun doWork(): Result {
        getLastKnownLocation(applicationContext)
        return Result.success()
    }
}