package org.pcc.xmodesocial.workmanager

import android.Manifest
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import org.pcc.xmodesocial.BuildConfig
import org.pcc.xmodesocial.MainActivity.Companion.PERMISSION_LOCATION
import org.pcc.xmodesocial.R
import java.time.LocalDateTime

object LocationHelper {

    //Task#3 - Retrieve last location
    fun getLastKnownLocation(context: Context) {
        //I used fused location because it gives provides location using both GPS and network providers/WiFi.
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        //Re-verifying permission just before actually retrieving location.
        // (User could've changed permission)
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    //Last location retrieved. Covering rare case when location is null
                    if (location != null) {
                        //Display location object
                        val message = "Last location object: $location"

                        //Task#3: Toast last location
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show()

                        //[Debugging purpose] Adding this notification to make sure that periodic task is working
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            debugStatusNotification(
                                context,
                                LocalDateTime.now().minute.toString() + "::Debug-" + BuildConfig.DEBUG +"."+ message
                            )
                        }

                        //Task #4 - Local storage options
                        //TODO :
                        //Option #1: Shared Prefs when dataset is smaller about 1MB-2MB
                        //Option #2: For larger data prefer local SQLite based database using Room
                    }
                }
        }
    }

    //Check for location permissions
    fun checkPermissions(context: Context): Boolean {
        return (ContextCompat.checkSelfPermission(context,
            Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
    }

    /**
     * Task#3: Getting user's consents
     * This function can be moved into Permission Helper class as an enhancement
     */
    fun askForConsent(context: Activity) {
        if (!checkPermissions(context)) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(context as Context, "Not sufficient permissions", Toast.LENGTH_LONG).show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(context,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_LOCATION)
            }
        }
    }

    /**
     * This status notification is triggered when last location is retrieved.
     * First part of the notification displays the minutes of the hour when it's triggered.
     * Second part shows whether build is in Debug mode or Release mode.
     * (Location retrieval duration is 1 hour in Release mode)
     * Third part shows the Last location object (toString())
     */
    private fun debugStatusNotification(context: Context, message: String) {
        val channelId = "PERIODIC_NOTIFICATION"
        val notificationDesc = "Last location retriever "
        val notificationChannelName = "Location Worker"
        val notificationTitle = "Last Location"
        val notificationId = 1
        // Make a channel if necessary
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            val name = notificationChannelName
            val description = notificationDesc
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, name, importance)
            channel.description = description

            // Add the channel
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channel)
        }

        // Create the notification
        val builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(notificationTitle)
            .setContentText(message)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(LongArray(0))

        // Show the notification
        NotificationManagerCompat.from(context).notify(notificationId, builder.build())
    }
}