package org.pcc.xmodesocial.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.BOOT_COMPLETED"
            || intent.action == "android.intent.action.MY_PACKAGE_REPLACED") {
            AlarmHelper.enableAlarm(context)
        }
    }
}