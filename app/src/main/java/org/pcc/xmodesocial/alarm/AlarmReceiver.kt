package org.pcc.xmodesocial.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import org.pcc.xmodesocial.workmanager.LocationHelper

/**
 * AlarmReceiver handles the broadcast message and generates Notification
 */
class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Toast.makeText(context, "Alarm received.", Toast.LENGTH_LONG).show()
        LocationHelper.getLastKnownLocation(context)
    }
}