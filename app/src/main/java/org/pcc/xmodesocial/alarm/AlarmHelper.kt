package org.pcc.xmodesocial.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.os.SystemClock

object AlarmHelper {

    var ALARM_TYPE_ELAPSED = 101
    private var alarmManagerElapsed: AlarmManager? = null
    private var alarmIntentElapsed: PendingIntent? = null


    //TASK #2: Set time interval to 1 minute
    /***
     * This is another way to schedule notifications using the elapsed time.
     * Its based on the relative time since device was booted up.
     * @param context
     */
    fun scheduleRepeatingElapsedLocationWork(context: Context) {
        //Setting intent to class where notification will be handled
        val intent = Intent(context, AlarmReceiver::class.java)

        //Setting pending intent to respond to broadcast sent by AlarmManager everyday at 8am
        alarmIntentElapsed = PendingIntent.getBroadcast(context, ALARM_TYPE_ELAPSED, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        //getting instance of AlarmManager service
        alarmManagerElapsed = context.getSystemService(ALARM_SERVICE) as AlarmManager

        //Inexact alarm everyday since device is booted up. This is a better choice and
        //scales well when device time settings/locale is changed
        //I'm setting alarm to fire notification every minute. However, it's not recommended to do so
        //because it'll keep waking the device and that will be very insufficient for battery

        alarmManagerElapsed!!.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000 * 60,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntentElapsed)
    }


    fun cancelAlarmElapsed() {
        if (alarmManagerElapsed != null) {
            alarmManagerElapsed!!.cancel(alarmIntentElapsed)
        }
    }

    /**
     * Enable boot receiver to persist alarms set for notifications across device reboots
     */
    fun enableBootReceiver(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.packageManager

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)
    }


    /**
     * Disable boot receiver when user cancels/opt-out from notifications
     */
    fun disableBootReceiver(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.packageManager

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP)
    }

    fun enableAlarm(applicationContext: Context) {
        scheduleRepeatingElapsedLocationWork(applicationContext)
        enableBootReceiver(applicationContext)
    }
}
